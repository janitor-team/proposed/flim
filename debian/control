Source: flim
Section: lisp
Priority: optional
Maintainer: Tatsuya Kinoshita <tats@debian.org>
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: texinfo
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debian/flim
Vcs-Git: https://salsa.debian.org/debian/flim.git
Homepage: https://github.com/wanderlust/flim
Rules-Requires-Root: no

Package: flim
Architecture: all
Depends: emacsen-common (>= 2.0.8), emacs-nox | emacs | emacs25 | emacs24 | emacs-snapshot, apel (>= 10.7), dpkg (>= 1.15.4) | install-info, ${misc:Depends}
Suggests: xemacs21-bin, semi, wl | wl-beta
Breaks: wl (<< 2.15.9+0.20190205-5~), wl-beta (<< 2.15.9+0.20200822-1~), mu-cite (<< 8.1+0.20201103-1~), semi (<< 1.14.7~0.20201104-1~)
Conflicts: tm, semi-gnus (<< 1:6.10.13), flim1.13
Replaces: flim1.13
Description: library about internet message for emacsen
 FLIM (Faithful Library about Internet Message) is a library to provide
 basic functions about message representation and encoding for emacsen.
 It consists of following modules:
 .
  std11.el         STD 11 (RFC 822) parser and utility
  mime.el          to provide various services about MIME-entities
  mime-def.el      Definitions about MIME format
  mime-parse.el    MIME parser
  mel.el           MIME encoder/decoder
  mel-q-ccl.el     quoted-printable and Q-encoding encoder/decoder (using CCL)
  mel-q.el         quoted-printable and Q-encoding encoder/decoder
  mel-u.el         unofficial backend for uuencode
  mel-g.el         unofficial backend for gzip64
  eword-decode.el  encoded-word decoder
  eword-encode.el  encoded-word encoder
 .
 This package provides FLIM-LB, a variant of FLIM using lexical binding
 mainly maintained for Wanderlust mail/news reader.
