flim (1:1.14.9+0.20201117-2) unstable; urgency=medium

  * Update Info handling to use makeinfo
  * Update debian/upstream/metadata to use https
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 06 Feb 2021 18:38:57 +0900

flim (1:1.14.9+0.20201117-1) unstable; urgency=medium

  * New upstream version 1.14.9+0.20201117
    - Fix incorrect std11-narrow-to-header breaks wl-template-select

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 18 Nov 2020 00:05:05 +0900

flim (1:1.14.9+0.20201111-1) unstable; urgency=medium

  * New upstream version 1.14.9+0.20201111
    - Fix luna.el fails with void-variable in lexical binding environment

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 12 Nov 2020 00:14:34 +0900

flim (1:1.14.9+0.20201029-1) unstable; urgency=medium

  * Switch Homepage to github.com/wanderlust/flim
  * Add the get-orig-source target
  * New upstream version 1.14.9+0.20201024
  * Drop debian/patches
  * Drop xemacs21, emacs23 and older versions
  * Update Texinfo files handling
  * Add debian/upstream/metadata
  * Update debian/watch to use mode=git
  * Update description to mention FLIM-LB
  * Update debian/copyright
  * Don't install obsolete ChangeLog
  * Update debhelper-compat to 13
  * Add Breaks to prevent failure with xemacs21
  * New upstream version 1.14.9+0.20201029

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 06 Nov 2020 21:26:57 +0900

flim (1:1.14.9+0.20120428-24) unstable; urgency=medium

  * Update 010_flim-1_14-wl.patch to 2020-03-06
  * Add Rules-Requires-Root: no
  * Update debian/copyright
  * Update Standards-Version to 4.5.0

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 07 Mar 2020 10:44:38 +0900

flim (1:1.14.9+0.20120428-23) unstable; urgency=medium

  * Update debhelper-compat to 12
  * Update Standards-Version to 4.4.0
  * Skip old flavors xemacs20 and xemacs19
  * Recompile semi and then w3m-el

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 16 Jul 2019 22:09:16 +0900

flim (1:1.14.9+0.20120428-22) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces

  [ Tatsuya Kinoshita ]
  * Update 010_flim-1_14-wl.patch to 2019-05-26
  * Update Standards-Version to 4.3.0
  * Update debian/copyright
  * Use debhelper-compat 11

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 07 Jul 2019 22:42:05 +0900

flim (1:1.14.9+0.20120428-21) unstable; urgency=medium

  * Fix handling for xemacs21-mule/nomule
  * Check stamp file so that source file isn't newer
  * Handle symlinks for emacsen-startup
  * Update Standards-Version to 4.2.0

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 19 Aug 2018 23:03:30 +0900

flim (1:1.14.9+0.20120428-20) unstable; urgency=medium

  * Accept unversioned emacs flavor for emacsen-common 3.0.0
  * Drop workaround for emacsen-common 1.x
  * Update debhelper compat version to 11
  * Update Standards-Version to 4.1.4
  * Don't install *.sgml

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 03 Jun 2018 17:51:30 +0900

flim (1:1.14.9+0.20120428-19) unstable; urgency=medium

  * Update 010_flim-1_14-wl.patch to 2018-03-29

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 30 Mar 2018 00:08:15 +0900

flim (1:1.14.9+0.20120428-18) unstable; urgency=medium

  * Update 010_flim-1_14-wl.patch to 2018-01-18
  * Update debhelper compat version to 10
  * Migrate from anonscm.debian.org to salsa.debian.org
  * Update debian/copyright
  * Update Standards-Version to 4.1.3
  * Don't byte-compile sasl-xoauth2.el

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 24 Jan 2018 23:15:39 +0900

flim (1:1.14.9+0.20120428-17) unstable; urgency=medium

  * Update 010_flim-1_14-wl.patch to 2016-12-11
  * Update debian/copyright
  * Drop 020_xemacs.patch (unneeded)
  * Prefer emacs-nox over emacs

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 05 Jan 2017 23:47:46 +0900

flim (1:1.14.9+0.20120428-16) unstable; urgency=medium

  * New patch 020_xemacs.patch to prevent errors on XEmacs
  * Update debhelper compat version to 9

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 28 Oct 2016 20:29:03 +0900

flim (1:1.14.9+0.20120428-15) unstable; urgency=medium

  * Update 010_flim-1_14-wl.patch to 2016-08-31
  * Accept emacs25

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 09 Oct 2016 13:33:10 +0900

flim (1:1.14.9+0.20120428-14) unstable; urgency=medium

  * Update 010_flim-1_14-wl.patch to 2016-03-12
  * Update debian/copyright
  * Update Standards-Version to 3.9.8

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 04 Jun 2016 20:18:14 +0900

flim (1:1.14.9+0.20120428-13) unstable; urgency=medium

  * Update 010_flim-1_14-wl.patch to 2015-12-13
  * Update Vcs-Browser to https

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 26 Dec 2015 18:39:00 +0900

flim (1:1.14.9+0.20120428-12) unstable; urgency=medium

  * Update 010_flim-1_14-wl.patch to 2015-07-30

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 09 Aug 2015 12:29:03 +0900

flim (1:1.14.9+0.20120428-11) unstable; urgency=medium

  * Update 010_flim-1_14-wl.patch to 2015-05-02
    - Fix for CCL unusable environments (closes: #783713)

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 03 May 2015 10:56:34 +0900

flim (1:1.14.9+0.20120428-10) unstable; urgency=medium

  * Update 010_flim-1_14-wl.patch to 2015-04-28
  * Update debian/copyright
  * Update Vcs-Browser
  * Update Standards-Version to 3.9.6

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 28 Apr 2015 18:26:20 +0900

flim (1:1.14.9+0.20120428-9) unstable; urgency=medium

  * Sync 010_flim-1_14-wl.patch on 2014-09-15
    - Workaround to handle '%s' in /etc/mailcap (closes: #572993)

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 16 Sep 2014 23:03:24 +0900

flim (1:1.14.9+0.20120428-8) unstable; urgency=medium

  * Sync 010_flim-1_14-wl.patch on 2014-06-26

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 28 Jun 2014 07:10:29 +0900

flim (1:1.14.9+0.20120428-7) unstable; urgency=medium

  * Sync 010_flim-1_14-wl.patch on 2014-06-16
  * Drop 020_ikazuhiro.patch (merged flim-1_14-wl)
  * Depend on emacsen-common 2.0.8

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 16 Jun 2014 20:05:40 +0900

flim (1:1.14.9+0.20120428-6) unstable; urgency=medium

  * Do not install obsolete attic/*.el files

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 21 May 2014 19:45:42 +0900

flim (1:1.14.9+0.20120428-5) unstable; urgency=medium

  * Sync 020_ikazuhiro.patch on 2014-04-23

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 23 Apr 2014 20:45:24 +0900

flim (1:1.14.9+0.20120428-4) unstable; urgency=medium

  * Update 020_ikazuhiro.patch, sync on 2014-02-22
  * Install a compat file with emacsen-compat
  * Add emacsen-common (<< 2.0.0) to Conflicts
  * Handle an installed file to follow emacsen-common 2.0.7
  * Byte-compile rail for reverse dependency
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 01 Mar 2014 20:46:56 +0900

flim (1:1.14.9+0.20120428-3) unstable; urgency=low

  * Update 020_ikazuhiro.patch, sync on 2013-07-06
  * Remove 030_Upstream-not-in-flim-1_14-wl.patch (merged ikazuhiro)
  * Workaround for emacsen-common <2 and debhelper <9.20131104
  * Update Standards-Version to 3.9.5

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 08 Dec 2013 23:05:52 +0900

flim (1:1.14.9+0.20120428-2) unstable; urgency=low

  * New patch 010_flim-1_14-wl.patch, sync with
    https://github.com/wanderlust/flim/commits/flim-1_14-wl on 2013-07-01
  * Remove 000*.patch (merged flim-1_14-wl)
  * New patch 020_ikazuhiro.patch, sync with
    https://github.com/ikazuhiro/flim/commits/flim-1_14-wl on 2013-07-05
  * New patch 030_Upstream-not-in-flim-1_14-wl.patch

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 05 Jul 2013 23:53:02 +0900

flim (1:1.14.9+0.20120428-1) unstable; urgency=low

  * Imported Upstream version 1.14.9+0.20120428
  * New patches from flim-1_14-wl
    - 0001-Use-new-style-backquotes.patch
    - 0002-Proper-ChangeLog-entry.patch
    - 0003-Use-new-style-backquotes-in-md4.el.patch
  * Remove 010_mel-q-ccl-for-emacs24.patch (unneeded now)
  * Add emacs24 to Build-Depends-Indep
  * Add Vcs-Git and Vcs-Browser
  * Switch Homepage to chise.org
  * Update debian/watch
  * Update debian/copyright
  * debian/rules: New targets build-arch and build-indep
  * Update Standards-Version to 3.9.4

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 11 May 2013 18:48:31 +0900

flim (1:1.14.9+0.20110516-1) unstable; urgency=low

  * New upstream release. (CVS flim-1_14-wl branch on 2011-05-16)
  * debian/patches/020_backquotes-for-emacs24.patch: Removed.
    (merged upstream)
  * debian/copyright: Use GFDL-NIV for GFDL with no invariant sections.

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 28 May 2011 21:10:48 +0900

flim (1:1.14.9+0.20101116-1) unstable; urgency=low

  * New upstream release. (CVS flim-1_14-wl branch on 2010-11-16)
  * debian/patches/020_backquotes-for-emacs24.patch: Remove the patch of
    md5-el.el. (merged upstream)
  * debian/control: Update Standards-Version to 3.9.2.
  * debian/copyright: Switch to the DEP-5 format.

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 17 May 2011 01:15:38 +0900

flim (1:1.14.9+0.20100805-1) unstable; urgency=low

  * New upstream release. (CVS flim-1_14 branch on 2010-08-05)
  * debian/patches/020_backquotes-for-emacs24.patch: Workaround for broken
    old style backquotes in Emacs 24.0.50. (closes: #588189)
  * debian/control:
    - Add GNU Emacs flavors to Build-Depends-Indep. (closes: #588435)
    - Update Standards-Version to 3.9.1.

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 16 Oct 2010 01:04:24 +0900

flim (1:1.14.9+0.20100313-2) unstable; urgency=low

  * debian/patches/01mel-q-ccl-for-emacs24.patch: Workaround for broken ccl
    encoding/decoding in the current emacs-snapshot package.

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 24 Mar 2010 00:24:24 +0900

flim (1:1.14.9+0.20100313-1) unstable; urgency=low

  * New upstream release. (CVS flim-1_14 branch on 2010-03-13)
  * debian/copyright: Add a location for the snapshot tarball.
  * Switch to dpkg-source 3.0 (quilt) format.

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 13 Mar 2010 23:02:27 +0900

flim (1:1.14.9+0.20100218-1) unstable; urgency=low

  * New upstream release. (CVS flim-1_14 branch on 2010-02-18)
  * debian/patches: Removed. (merged upstream)
  * Switch to dpkg-source 1.0 format.

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 18 Feb 2010 22:45:55 +0900

flim (1:1.14.9+0.20100210-2) unstable; urgency=low

  * debian/patches/wl-en-03503-20100210.patch: Patch from the upstream
    mailing list [wl-en:03503] on 2010-02-10 to fix incorrect usage of the
    8BITMIME smtp extension by Tetsurou Okazaki. (closes: #569223)
  * Switch to dpkg-source 3.0 (quilt) format.

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 13 Feb 2010 18:24:19 +0900

flim (1:1.14.9+0.20100210-1) unstable; urgency=low

  * New upstream release. (CVS flim-1_14 branch on 2010-02-10)
  * debian/rules, debian/control: Remove the quilt dependency.
  * debian/patches, debian/README.source: Removed.

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 10 Feb 2010 22:38:21 +0900

flim (1:1.14.9-5) unstable; urgency=low

  * debian/emacsen-startup, debian/control: Don't use metamail's mimencode,
    and suggest xemacs21-bin for mmencode. (closes: #562846)
  * debian/rules: Add direntry to *.texi for *.info. (closes: #528871)
  * debian/prerm, debian/postinst: Removed.
  * debian/control:
    - Add `dpkg (>= 1.15.4) | install-info' to Depends.
    - Add `${misc:Depends}' to Depends.
    - Set Section to lisp.
    - Update Standards-Version to 3.8.4.
  * debian/README.source: New file, mention that this package uses quilt to
    manage debian/patches.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 08 Feb 2010 23:46:23 +0900

flim (1:1.14.9-4.1) unstable; urgency=low

  * Non-maintainer upload.
  * Drop Recommends on metamail (which is now gone)

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 21 Jan 2010 22:53:30 +0100

flim (1:1.14.9-4) unstable; urgency=low

  * debian/emacsen-install: Don't fail with xemacs21-nomule.
    (closes: #522801)

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 08 Apr 2009 23:55:11 +0900

flim (1:1.14.9-3) unstable; urgency=low

  * debian/patches/cvs20081125.patch: Patch from the upstream CVS flim-1_14
    branch on 2008-11-25 for eword-decode.el, tests/test-rfc2231.el and
    mel-q-ccl.el by Katsumi Yamaoka and Yoichi NAKAYAMA.
  * debian/patches/yamaoka20080403.patch: Patch from [emacs-mime-ja:02201]
    on 2008-04-03 for mel-q-ccl.el to prevent a Quoted-Printable problem by
    Katsumi Yamaoka.
  * debian/patches/kzhr20071128.patch: Patch from [emacs-mime-ja:02196] on
    2007-11-28 for mel-q-ccl.el to prevent a Quoted-Printable problem by
    Kazuhiro Ito.
  * debian/rules, debian/control: Handle debian/patches with quilt.
  * debian/emacsen-*: Rewritten.
  * debian/rules, debian/dirs: Install obsolete files in the attic directory.
  * debian/docs, debian/rules: Install FLIM-API*, *.texi and *.sgml in the
    doc directory.
  * debian/rules: Use dh_prep instead of `dh_clean -k'.
  * debian/rules, debian/env.el: Don't use env.el.
  * debian/compat, debian/control: Update debhelper version to 7.
  * debian/control:
    - Remove `make' from `Depends:'.
    - Update Standards-Version to 3.8.1.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 04 Apr 2009 23:27:54 +0900

flim (1:1.14.9-2) unstable; urgency=low

  * debian/emacsen-startup: Set base64-internal-decoding-limit and
    base64-internal-encoding-limit to 0 for emacs-unicode, suggested by
    Clemens Fruhwirth, [wl-en:02341] on 2008-03-28.
  * debian/control:
    - Move `Homepage:' from Description to the header.
    - Set Standards-Version to 3.7.3.
    - Build-Depends: debhelper (>= 6).
  * debian/compat: 5 -> 6.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 28 Mar 2008 22:10:59 +0900

flim (1:1.14.9-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 28 Nov 2007 22:05:56 +0900

flim (1:1.14.8+0.20070618-4) unstable; urgency=low

  * debian/emacsen-startup: Use (charsetp 'unicode-bmp) instead of checking
    emacs-major-version to detect the emacs-unicode-2 branch of Emacs.

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 02 Sep 2007 13:07:20 +0900

flim (1:1.14.8+0.20070618-3) unstable; urgency=low

  * debian/control:
    - Move semi from Recommends to Suggests.
    - Remove t-gnus from Suggests, which no longer exists in Debian.
  * debian/emacsen-startup: Set mel-b-ccl-module to nil for Emacs 23
    to prevent a decoding problem.  (Thanks to Clemens Fruhwirth,
    [wl-en:02175] on 2007-08-23)
  * debian/copyright: Refer to /usr/share/common-licenses/GFDL.

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 24 Aug 2007 21:05:15 +0900

flim (1:1.14.8+0.20070618-2) unstable; urgency=low

  * debian/rules: Use emacs instead of emacs21.

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 09 Jul 2007 21:51:13 +0900

flim (1:1.14.8+0.20070618-1) unstable; urgency=low

  * New upstream release. (CVS flim-1_14 branch on 2007-06-18)
  * debian/control: Prefer emacs to emacs21.

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 23 Jun 2007 20:38:50 +0900

flim (1:1.14.8+0.20061220-1) unstable; urgency=low

  * New upstream release. (CVS flim-1_14 branch on 2006-12-20)
  * debian/emacsen-install.in: Recompile w3m-el.
  * debian/prerm: Check whether the argument is "remove" or "upgrade".
  * debian/control:
    - Depends apel (>= 10.7).
    - Add `Homepage:' to Description.
  * debian/watch: Set Action to uupdate.
  * debian/copyright:
    - Add license of sasl.texi.
    - Reformat copyright years.
    - Mention Debian packaging conditions.

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 08 Apr 2007 19:47:40 +0900

flim (1:1.14.8+0.20060617-1) unstable; urgency=low

  * New upstream release. (CVS flim-1_14 branch on 2006-06-17, without
    sasl.texi)

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 17 Jun 2006 13:20:54 +0900

flim (1:1.14.8+0.20060218.0226-2) unstable; urgency=low

  * debian/emacsen-startup: Use metamail's commands instead of mime-codecs'
    commands. (mime-codecs no longer exists in the archive.)
  * debian/control: Recommends metamail instead of mime-codecs.

 -- Tatsuya Kinoshita <tats@debian.org>  Thu,  8 Jun 2006 01:53:39 +0900

flim (1:1.14.8+0.20060218.0226-1) unstable; urgency=low

  * Re-create .orig.tar.gz. (Removed debian/* files from .orig.tar.gz)

 -- Tatsuya Kinoshita <tats@debian.org>  Wed,  7 Jun 2006 23:15:12 +0900

flim (1:1.14.8+0.20060218-1) unstable; urgency=low

  * New upstream release. (CVS flim-1_14 branch on 2006-02-18, without
    sasl.texi)
  * debian/control (Build-Depends): Depend on debhelper version 5.
  * debian/control (Standards-Version): 3.6.2 -> 3.7.2.

 -- Tatsuya Kinoshita <tats@debian.org>  Wed,  7 Jun 2006 20:33:40 +0900

flim (1:1.14.8+0.20051225-1) unstable; urgency=low

  * New upstream release. (CVS flim-1_14 branch on 2005-12-25, without
    sasl.texi)
  * debian/control (Depends): Add `emacsen'. (closes: #321615)
  * debian/control (Build-Depends-Indep): Depend on debhelper version 5.
  * debian/compat: 3 -> 5.
  * debian/control (Standards-Version): 3.6.1 -> 3.6.2.
  * debian/copyright: Update the postal address of the Free Software
    Foundation.
  * debian/control (Maintainer): tats@vega.ocn.ne.jp -> tats@debian.org.
  * debian/copyright: Ditto.

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 23 Jan 2006 01:31:32 +0900

flim (1:1.14.7+0.20041120-1) unstable; urgency=low

  * New upstream release. (CVS flim-1_14 branch on 2004-11-20, without
    sasl.texi)
  * debian/env.el: Prefer iso-2022-jp instead of euc-jp.
  * debian/emacsen-install.in: Ready for emacsen flavors sxemacs*.
  * debian/watch: New file.
  * debian/control: Revise description.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sat,  4 Jun 2005 14:37:31 +0900

flim (1:1.14.6+0.20040609-1) unstable; urgency=low

  * New upstream release. (CVS snapshot on 2004-06-09 at 01:17 +0900)
    - std11.el (std11-parse-word-or-comment-or-period): Renamed from
      `std11-parse-word-or-comment' and allow period.
      (std11-parse-phrase): Allow parsing obs-phrase in rfc2822.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Wed,  9 Jun 2004 01:19:49 +0900

flim (1:1.14.6+0.20040525-1) unstable; urgency=low

  * New upstream release. (CVS snapshot on 2004-05-25 at 20:40 +0900)
    - mime-conf.el (mime-format-mailcap-command): Quote a file name
      which may contain spaces using `shell-quote-argument' in order to
      pass it to the shell safely.
    - eword-encode.el (mime-encode-header-in-buffer): Don't ignore a
      space char at the beginning of a field body in order to honor the
      76-column rule which is described in RFC 2047 section 2.
  * debian/emacsen-install.in: Create *.el symlinks.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Tue, 25 May 2004 20:48:54 +0900

flim (1:1.14.6+0.20040415-1) unstable; urgency=high

  * New upstream release. (CVS snapshot on 2004-04-15 at 22:38 +0900)
    - mel-u.el (mime-write-decoded-region): Use make-temp-file.
      (uuencode-external-decode-region): Ditto.
    - eword-decode.el (eword-decode-encoded-text): Fix bug when
      `must-unfold' is specified; eliminate CR too when `must-unfold' is
      specified.
  * debian/control (Depends): `apel (>= 10.5)' to use make-temp-file.
  * debian/changelog: Encoded with UTF-8.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sun, 18 Apr 2004 00:28:55 +0900

flim (1:1.14.6+0.20040306-1) unstable; urgency=low

  * New upstream release. (CVS snapshot on 2004-03-06 at 20:14 +0900)
    - lunit.el (lunit-make-test-suite-from-class): Fix regexp.
      Use "^test" instead of "^test-".

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sat,  6 Mar 2004 23:46:40 +0900

flim (1:1.14.6+0.20040219-1) unstable; urgency=low

  * New upstream release. (CVS snapshot on 2004-02-19 at 23:45 +0900)
    - New user option `smtp-starttls-program' and `smtp-starttls-extra-args'.
  * debian/control (Depends): Remove `emacs20'. (closes: #232758)

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sun, 22 Feb 2004 22:18:40 +0900

flim (1:1.14.6+0.20040127-1) unstable; urgency=low

  * New upstream release. (CVS snapshot on 2004-01-27 at 23:34 +0900)
    - Apply patches from Gnus.
    - `quoted-printable-external-decoder-option-to-specify-file' and
      `base64-external-decoder-option-to-specify-file' accept nil for
      using stdout.
  * debian/emacsen-startup: Use mime-codecs' commands instead of metamail's
    commands. (closes: #212342)
  * debian/control: Recommends mime-codecs instead of metamail.
  * sasl.texi: Removed.  It will be included in Gnus.
  * debian/copyright: Revised.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Wed, 28 Jan 2004 00:30:53 +0900

flim (1:1.14.6-1) unstable; urgency=low

  * New upstream release.
    - New option, `mime-header-accept-quoted-encoded-words'.
    - Add entry for iso-8859-1[45].
  * debian/copyright: Further clarification.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sun, 21 Dec 2003 00:16:19 +0900

flim (1:1.14.5+0.20031113-1) unstable; urgency=low

  * New upstream release. (CVS snapshot on 2003-11-13 at 23:20 +0900)
    - Don't install smtpmail which conflicts with Emacs's smtpmail.
    - Fix that mel-b-el.elc is not installed.
  * debian/00patch*: Removed. (merged into the upstream)
  * debian/emacsen-startup: On XEmacs, use "mmencode" (default value)
    instead of "mimencode". (The mmencode command is provided by the
    xemacs21-bin package.)
  * debian/emacsen-startup: Add comments for the mime-codecs' commands.
    (relate to Bug#212342)
  * debian/emacsen-startup: Add configuration for
    base64-external-decoder-option-to-specify-file,
    quoted-printable-internal-encoding-limit, and
    quoted-printable-external-decoder-option-to-specify-file.
  * debian/rules: Obsolete files are installed in /usr/share/flim instead
    of the site-lisp directory.
  * debian/dirs: Add `usr/share/flim'.
  * debian/control (Build-Depends-Indep): Depend on emacs21 instead of
    emacs20.
  * debian/rules: Use emacs21 instead of emacs20.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Thu, 13 Nov 2003 23:26:01 +0900

flim (1:1.14.5+0.20030430-4) unstable; urgency=low

  * debian/rules: Don't install smtpmail.el which conflicts with Emacs's
    smtpmail.el. (This change may prevent gnus conflict)
  * debian/control (Description): Remove description of gnus conflict.
    (closes: #208978)
  * debian/control (Description): Add more information.
  * Use debian/compat instead of DH_COMPAT.
    - debian/compat: New file.
    - debian/rules: Remove `export DH_COMPAT=3'.
    - debian/control (Build-Depends-Indep): debhelper (>= 3.4.4).
  * debian/control (Standards-Version): 3.6.0 -> 3.6.1.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Fri, 12 Sep 2003 00:44:34 +0900

flim (1:1.14.5+0.20030430-3) unstable; urgency=low

  * FLIM-ELS: Fix that mel-b-el.elc is not installed. (patch from
    [Wanderlust English:00451], [emacs-mime-en:00104])
  * debian/emacsen-install.in: Set *.elc time stamp correctly.
  * debian/copyright: Add upstream URL for FTP.
  * debian/control (Standards-Version): 3.5.10 -> 3.6.0.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Mon, 18 Aug 2003 07:56:23 +0900

flim (1:1.14.5+0.20030430-2) unstable; urgency=low

  * Revise installation scripts.
  * Ready for xemacs21-{mule,nomule} flavors.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sun,  6 Jul 2003 22:18:50 +0900

flim (1:1.14.5+0.20030430-1) unstable; urgency=low

  * New upstream release (1.14.5 + patch from cvs.m17n.org, flim-1_14
    branch of flim, last modified 2003-04-30, closes: #198869)
  * Rewrite Info files installation. (closes: #170017)
  * debian/control: Build-depend on emacs20 instead of emacsen.
  * debian/control: Use Build-Depends-Indep instead of Build-Depends.
  * debian/rules: Use binary-indep instead of binary-arch.
  * debian/rules: Don't use dh_suidregister which is obsolete.
  * debian/rules: Don't leave install-stamp when `debuild clean'.
  * debian/copyright: Revised.
  * debian/control: Prefer emacs21 instead of emacs20.
  * debian/control: Remove `chaos' from `Suggests:'.
  * debian/control: Revise `Description:'.
  * debian/control: Standards-Version: 3.5.2 -> 3.5.10.
  * debian/env.el: Use prefer-coding-system instead of
    set-default-coding-systems.
  * debian/emacsen-startup: Use debian-pkg-add-load-path-item.
  * debian/prerm: Renamed from debian/postrm.
  * debian/README.debian: Removed.
  * Reject Debian patches for FLIM-ELS and mime-en.texi.
  * Creanup installation scripts.
  * New maintainer. (with previous maintainer's consent)

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Tue,  1 Jul 2003 22:36:44 +0900

flim (1:1.14.4-1) unstable; urgency=low

  * New upstream release.
  * Change Maintainer address to @debian.org

 -- Takuo KITAME <kitame@debian.org>  Wed, 23 Oct 2002 14:27:26 +0900

flim (1:1.14.3-10) unstable; urgency=low

  * fix description (closes: #131414)

 -- Takuo KITAME <kitame@northeye.org>  Wed, 24 Apr 2002 10:12:19 +0900

flim (1:1.14.3-9) unstable; urgency=low

  * fix nomule problem.

 -- Takuo KITAME <kitame@northeye.org>  Fri, 11 Jan 2002 00:30:48 +0900

flim (1:1.14.3-8) unstable; urgency=low

  * debian/control:
    - build-depends: - emacs20, + emacsen
  * debian/rules:
    - use debian/env.el to make japanese info.

 -- Takuo KITAME <kitame@northeye.org>  Wed, 19 Dec 2001 11:38:12 +0900

flim (1:1.14.3-7) unstable; urgency=low

  * remove unnecessary .el (closes: #123574)
  * fix comment mismatch in 50flim.el (closes: #123582)

 -- Takuo KITAME <kitame@northeye.org>  Wed, 12 Dec 2001 20:52:06 +0900

flim (1:1.14.3-6) unstable; urgency=low

  * /etc/emacs/site-start.d/50flim.el as conffile

 -- Takuo KITAME <kitame@northeye.org>  Tue, 11 Dec 2001 20:21:25 +0900

flim (1:1.14.3-5) unstable; urgency=low

  * remove conflicts: gnus (currentry, mailcap.el will not be installed)

 -- Takuo KITAME <kitame@northeye.org>  Fri, 23 Nov 2001 14:53:34 +0900

flim (1:1.14.3-4) unstable; urgency=low

  * use md5-el.el instead of md5-dl.el on emacs20-dl (closes: #119593)

 -- Takuo KITAME <takuo@debian.gr.jp>  Sun, 18 Nov 2001 04:26:20 +0900

flim (1:1.14.3-3) unstable; urgency=low

  * fix compile problem on xemacs21-nomule

 -- Takuo KITAME <kitame@northeye.org>  Sat, 10 Nov 2001 03:05:18 +0900

flim (1:1.14.3-2) unstable; urgency=low

  * doesn't support mule2 (closes: #111852)

 -- Takuo KITAME <kitame@northeye.org>  Wed, 12 Sep 2001 02:28:19 +0900

flim (1:1.14.3-1) unstable; urgency=low

  * New upstream release
  * don't install mailcap.el, it's obsolete. (closes: Bug#110224)

 -- Takuo KITAME <kitame@northeye.org>  Sun,  2 Sep 2001 01:11:58 +0900

flim (1:1.14.2.20010320cvs-1) unstable; urgency=low

  * New upstream release

 -- Takuo KITAME <kitame@northeye.org>  Wed, 21 Mar 2001 16:00:46 +0900

flim (1:1.14.2-1) unstable; urgency=low

  * New upstream release

 -- Takuo KITAME <kitame@northeye.org>  Tue, 26 Dec 2000 01:49:22 +0900

flim (1:1.14.1-1) unstable; urgency=low

  * New upstream release

 -- Takuo KITAME <kitame@northeye.org>  Mon, 25 Dec 2000 12:38:29 +0900

flim (1:1.14.0.20001223cvs-1) unstable; urgency=low

  * New upstream release

 -- Takuo KITAME <kitame@northeye.org>  Sun, 24 Dec 2000 02:37:49 +0900

flim (1:1.14.0.20001220cvs-1) unstable; urgency=low

  * New upstream release

 -- Takuo KITAME <kitame@northeye.org>  Wed, 20 Dec 2000 17:26:33 +0900

flim (1:1.14.0.20001215cvs-1) unstable; urgency=low

  * New upstream release

 -- Takuo KITAME <kitame@northeye.org>  Sat, 16 Dec 2000 13:06:41 +0900

flim (1:1.14.0.20001207cvs-1) unstable; urgency=low

  * New upstream release

 -- Takuo KITAME <kitame@northeye.org>  Fri,  8 Dec 2000 00:55:35 +0900

flim (1:1.14.0.20001204cvs-1) unstable; urgency=low

  * New upstream release

 -- Takuo KITAME <kitame@northeye.org>  Mon,  4 Dec 2000 22:11:54 +0900

flim (1:1.14.0.20001124cvs-1) unstable; urgency=low

  * New upstream release

 -- Takuo KITAME <kitame@northeye.org>  Tue, 28 Nov 2000 23:25:44 +0900

flim (1:1.14.0.20001120cvs-1) unstable; urgency=low

  * New upstream release

 -- Takuo KITAME <kitame@northeye.org>  Mon, 20 Nov 2000 19:38:59 +0900

flim (1:1.14.0.20001113cvs-1) unstable; urgency=low

  * New upstream release

 -- Takuo KITAME <kitame@northeye.org>  Tue, 14 Nov 2000 01:43:57 +0900

flim (1:1.14.0.20001108cvs-1) unstable; urgency=low

  * New upstream release (branch flim-1_14)

 -- Takuo KITAME <kitame@northeye.org>  Wed,  8 Nov 2000 18:24:28 +0900

flim (1:1.14+0.chao.1.14.1-1) unstable; urgency=low

  * New upstream release (branch chao-1_14)

 -- Takuo KITAME <kitame@northeye.org>  Mon, 23 Oct 2000 14:48:13 +0900

flim (1:1.12.7-14) frozen unstable; urgency=low

  * Fixed Dependency problem. emacsen-install needs 'make' command.

 -- Takuo KITAME <kitame@northeye.org>  Thu,  6 Apr 2000 07:37:41 +0900

flim (1:1.12.7-13) unstable; urgency=low

  * control: conflicts: gnus
  * control: Suggests: semi-gnus|wl

 -- Takuo KITAME <kitame@northeye.org>  Mon, 25 Oct 1999 20:56:52 +0900

flim (1:1.12.7-12) unstable; urgency=low

  * emacsen-common: modified.

 -- Takuo KITAME <kitame@northeye.org>  Sun, 24 Oct 1999 10:47:06 +0900

flim (1:1.12.7-11) unstable; urgency=low

  * emacsen-common: any FLAVOR allowed (except emacs)

 -- Takuo KITAME <kitame@northeye.org>  Tue, 19 Oct 1999 11:13:18 +0900

flim (1:1.12.7-10) unstable; urgency=low

  * emacsen-install: compile-stamp check.

 -- Takuo KITAME <kitame@northeye.org>  Thu, 14 Oct 1999 12:38:01 +0900

flim (1:1.12.7-9) unstable; urgency=low

  * emacsen-install: a little modified.
  * control: Standards-Version: 3.0.0

 -- Takuo KITAME <kitame@northeye.org>  Wed, 13 Oct 1999 22:19:35 +0900

flim (1:1.12.7-8) unstable; urgency=low

  * Modified flim-init.el

 -- Takuo KITAME <kitame@northeye.org>  Wed,  6 Oct 1999 22:44:31 +0900

flim (1:1.12.7-7) unstable; urgency=low

  * Fixed cannot byte-compile problem for xemacs20. (closes: Bug#45084)

 -- Takuro KITAME <kitame@northeye.org>  Thu, 16 Sep 1999 18:32:30 +0900

flim (1:1.12.7-6) unstable; urgency=low

  * Fixed cannot byte-compile problem. (closes: Bug#45084)
  * FHS compliance.

 -- Takuro KITAME <kitame@northeye.org>  Wed, 15 Sep 1999 01:26:17 +0900

flim (1:1.12.7-5) unstable; urgency=low

  * Fixed missing Recommends: ([s|w]emi -> [s|w]emi1.12).
  * Added Conflicts: tm

 -- Takuro KITAME <kitame@debian.or.jp>  Mon,  9 Aug 1999 05:00:10 +0900

flim (1:1.12.7-4) unstable; urgency=low

  * remove *.el files from FLAVOR/site-lisp/flim after byte-compile.

 -- Takuro KITAME <kitame@debian.or.jp>  Sun,  8 Aug 1999 15:42:49 +0900

flim (1:1.12.7-3) unstable; urgency=low

  * emacsen-{install,remove} scripts modified for xemacs21

 -- Takuro KITAME <kitame@debian.or.jp>  Sun, 20 Jun 1999 22:26:33 +0900

flim (1:1.12.7-2) unstable; urgency=low

  * Depends: apel(>=9.19) (Bug#39451).

 -- Takuro KITAME <kitame@debian.or.jp>  Mon, 14 Jun 1999 19:45:03 +0900

flim (1:1.12.7-1) unstable; urgency=low

  * New upstream release

 -- Takuro KITAME <kitame@debian.or.jp>  Tue,  1 Jun 1999 13:23:21 +0900

flim (1:1.12.6-2) unstable; urgency=low

  * Install info files.

 -- Takuro KITAME <kitame@debian.or.jp>  Thu, 20 May 1999 17:11:11 +0900

flim (1:1.12.6-1) unstable; urgency=low

  * New upstream release.

 -- Takuro KITAME <kitame@debian.or.jp>  Wed, 12 May 1999 03:56:56 +0900

flim (1:1.12.5-5) unstable; urgency=low (high for emacs19)

  * modified emacsen-install script for emacs19.

 -- Takuro KITAME <kitame@debian.or.jp>  Mon, 10 May 1999 16:14:22 +0900

flim (1:1.12.5-4) unstable; urgency=low (high for emacs19)

  * Work on emacs19 with 'custom'.

 -- Takuro KITAME <kitame@debian.or.jp>  Thu, 15 Apr 1999 10:42:26 +0900

flim (1:1.12.5-3) unstable; urgency=low

  * Byte compile log will be redirect to file.

 -- Takuro KITAME <kitame@debian.or.jp>  Tue,  2 Mar 1999 16:11:02 +0900

flim (1:1.12.5-2) unstable; urgency=low

  * Added
	  (setq quoted-printable-external-decoder '("mimencode" "-q" "-u"))
	  (setq quoted-printable-external-encoder '("mimencode" "-q"))
	  in /etc/emacs/site-start.d/50flim.el

 -- Takuro KITAME <kitame@debian.or.jp>  Tue, 16 Feb 1999 10:52:23 +0900

flim (1:1.12.5-1) unstable; urgency=low

  * New upstream release

 -- Takuro KITAME <kitame@debian.or.jp>  Thu, 28 Jan 1999 09:48:48 +0900

flim (1:1.12.4-1) unstable; urgency=low

  * New upstream release

 -- Takuro KITAME <kitame@debian.or.jp>  Mon, 25 Jan 1999 01:31:51 +0900

flim (1:1.12.3-1) unstable; urgency=low

  * New upstream release

 -- Takuro KITAME <kitame@debian.or.jp>  Mon, 25 Jan 1999 00:54:10 +0900

flim (1:1.12.2+19990111cvs-1) unstable; urgency=low

  * New upstream release. (CVS)

 -- Takuro KITAME <kitame@debian.or.jp>  Mon, 11 Jan 1999 13:41:59 +0900

flim (1:1.12.2-2) unstable; urgency=low

  * modified emacsen install script.

 -- Takuro KITAME <kitame@debian.or.jp>  Fri, 11 Dec 1998 22:27:12 +0900

flim (1:1.12.2-1) unstable; urgency=low

  * New upstream release
    (Development release Version)

 -- Takuro KITAME <kitame@debian.or.jp>  Mon, 16 Nov 1998 20:03:27 +0900

flim (1:1.12.1-1) unstable; urgency=low

  * New upstream release
    (Development release Version)

 -- Takuro KITAME <kitame@debian.or.jp>  Sat,  7 Nov 1998 13:48:36 +0900

flim (1:1.11.3-1) unstable; urgency=low

  * New upstream release

 -- Takuro KITAME <kitame@debian.or.jp>  Thu, 29 Oct 1998 13:03:52 +0900

flim (1:1.11.2-1) unstable; urgency=low

  * New upstream release

 -- Takuro KITAME <kitame@debian.or.jp>  Tue, 27 Oct 1998 13:24:10 +0900

flim (1:1.11.0-1) unstable; urgency=low

  * New upstream release
  * Fixed Bug#JP/572

 -- Takuro KITAME <kitame@debian.or.jp>  Tue, 20 Oct 1998 16:23:11 +0900

flim (1:1.10.5-1) unstable; urgency=low

  * New upstream release

 -- Takuro KITAME <kitame@debian.or.jp>  Thu, 15 Oct 1998 16:54:54 +0900

flim (1:1.9.2-4) unstable; urgency=low

  * Remove load-path mel in 50flim.el (Bug#JP/556)

 -- Takuro KITAME <kitame@debian.or.jp>  Tue, 22 Sep 1998 21:10:02 +0900

flim (1:1.9.2-3) unstable; urgency=low

  * changed package version 1.9.2-2 => 1:1.9.2-3

 -- Takuro KITAME <kitame@debian.or.jp>  Mon, 21 Sep 1998 21:21:09 +0900

flim (1.9.2-2) unstable; urgency=low

  * Deleted Provides: mel

 -- Takuro KITAME <kitame@debian.or.jp>  Sat, 19 Sep 1998 09:57:36 +0900

flim (1.9.2-1) unstable; urgency=low

  * New upstream release

 -- Takuro KITAME <kitame@debian.or.jp>  Tue, 15 Sep 1998 22:09:26 +0900

flim (1.9.1-1) unstable; urgency=low

  * New upstream release

 -- Takuro KITAME <kitame@debian.or.jp>  Mon, 14 Sep 1998 10:18:23 +0900

flim (1.9.0-1) unstable; urgency=low

  * New upstream release

 -- Takuro KITAME <kitame@debian.or.jp>  Thu, 16 Jul 1998 08:38:56 +0900

flim (1:1.8.0+chao-1) unstable; urgency=low

  * New upstream release (experimental)

 -- Takuro KITAME <kitame@debian.or.jp>  Mon, 13 Jul 1998 18:57:26 +0900

flim (1.8.1-1) unstable; urgency=low

  * New upstream release

 -- Takuro KITAME <kitame@debian.or.jp>  Wed,  8 Jul 1998 09:15:35 +0900

flim (1.8.0-1) unstable; urgency=low

  * New upstream release

 -- Takuro KITAME <kitame@debian.or.jp>  Thu,  2 Jul 1998 16:07:09 +0900

flim (1.6.0-2) unstable; urgency=low

  * site file changed (52 -> 50)

 -- Takuro KITAME <kitame@debian.or.jp>  Wed, 24 Jun 1998 23:03:44 +0900

flim (1.6.0-1) unstable; urgency=low

  * New upstream release

 -- Takuro KITAME <kitame@debian.or.jp>  Wed, 24 Jun 1998 23:03:44 +0900

flim (1.5.0-1) unstable-jp; urgency=low

  * New upstream release

 -- Takuro KITAME <kitame@debian.or.jp>  Wed, 24 Jun 1998 01:44:56 +0900

flim (1.3.0-1) unstable; urgency=low

  * new upstream version.

 -- Takuro KITAME <kitame@debian.or.jp>  Tue,  2 Jun 1998 22:20:07 +0900

flim (1.2.2-1) unstable; urgency=low

  * new upstream version.

 -- Takuro KITAME <kitame@debian.or.jp>  Mon, 18 May 1998 01:40:33 +0900

flim (1.2.1-1) unstable; urgency=low

  * new upstream version.

  Original changes follow.
  * FLIM: Version 1.2.1 (Kamitobaguchi) was released.
  * mel-q.el (quoted-printable-internal-decode-region): Use
    'string-as-multibyte to avoid problem in Emacs 20.3.
  * mel-u.el: Use mime-temp-directory instead of TMP.

 -- Takuro KITAME <kitame@debian.or.jp>  Sun, 10 May 1998 17:51:45 +0900

flim (1.2.0-1) unstable; urgency=low

  * new upstream version.

  Original changes follow.
   * FLIM: Version 1.2.0 (Jūjō) was released.
   * README.en (What's FLIM): Delete description about
     std11-parse.el; add description about mailcap.el.
   * eword-decode.el (eword-decode-encoded-word-error-handler): New
     variable.
     (eword-decode-encoded-word-default-error-handler): New function.
     (eword-decode-encoded-word): Use 'eword-decode-encoded-word-error-handler.
   * mailcap.el: Require 'mime-def.
   * mime-def.el (mime-type/subtype-string): New function (moved from
     semi/mime-parse.el).
   * std11-parse.el: Abolish std11-parse.el.
   * FLIM-ELS (flim-modules): Abolish 'std11-parse.
   * eword-decode.el: Require 'std11 instead of 'std11-parse.
   * std11.el: Merge std11-parse.el.
   * mime-def.el (mime-temp-directory): Use 'defcustom.
   * mel-u.el: Require 'mime-def instead of 'mel.
   * mime-def.el (mime-temp-directory): New variable (moved from mel.el).
   * mel.el: Move definition of 'mime-temp-directory to mime-def.el.
   * mailcap.el (mailcap-format-command): New function.
   * mailcap.el (mailcap-look-at-mtext): Don't strip quoted character again.

 -- Takuro KITAME <kitame@debian.or.jp>  Thu,  7 May 1998 00:56:11 +0900

flim (1.1.0-1) unstable; urgency=low

  * new upstream version.

  Original changes follow.
   * FLIM: Version 1.1.0 (Tōji) was released.
   * mime-def.el (quoted-printable-hex-chars): New constant.
     (quoted-printable-octet-regexp): New constant.
   * mel-q.el, eword-decode.el: Move definition of constant
     'quoted-printable-hex-chars and 'quoted-printable-octet-regexp to
     mime-def.el.
   * mailcap.el (mailcap-look-at-mtext): Strip quoted character.
   * mailcap.el (mailcap-look-at-mtext): Fix typo.
   * FLIM-ELS (flim-modules): Add mailcap.
   * mailcap.el: New file (copied from SEMI).
   * eword-decode.el (eword-decode-ignored-field-list): Add
     `received'.
   * mel.el (mime-temp-directory): Use TMPDIR, TMP, or TEMP
     environment variables.

 -- Takuro KITAME <kitame@debian.or.jp>  Tue,  5 May 1998 20:37:07 +0900

flim (1.0.1-1) unstable; urgency=low

  * Initial Release.

 -- Takuro KITAME <kitame@debian.or.jp>  Sun, 19 Apr 1998 23:50:08 +0900
